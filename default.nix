{ config, exports, home, lib, pkgs, ... }:

let

  inherit (config) domain;
  inherit (lib) mkOption;
  inherit (lib.types) str;

  leDir = "${home}/letsencrypt";
  logDir = "${home}/logs";
  nginxConfigLink = "${home}/nginx.conf";

  # Can't use fetchFromGitLab because of submodules
  src = pkgs.fetchgit {
    url = https://gitlab.com/phunehehe/foomail.git;
    rev = "99d25f05dcfdde5099cffff882f8ed217c8aead8";
    sha256 = "05gmkaxlqnvyq6spaa6ic0bdzj5qq3v539p7d2zskxprva90xnzk";
  };
  foomail = pkgs.callPackage src {};

  nginxConfig = pkgs.writeText "nginx.conf" ''

    server {

      server_name          ${domain};
      client_max_body_size 10M;

      # FIXME: ugly hard coded path
      root                 ${foomail}/share/x86_64-linux-ghc-8.0.1/foomail-0.0.0/static;

      access_log           ${logDir}/nginx-access.log bunyan;
      error_log            ${logDir}/nginx-error.log;

      ${exports.nginx.listenDualExtras 443 ["ssl" "http2"]}
      ${exports.nginx.staticFilesConfig}
      ${exports.nginx.hstsConfig}

      ${pkgs.leUtils.nginxSslPaths leDir}

      # TODO: maybe this should be an export in Nginx
      add_header Content-Security-Policy "script-src 'self'";

      location @foomail {
        proxy_pass http://localhost:8080;
      }

      try_files $uri $uri/ @foomail;
    }

    server {

      server_name ${domain};
      ${exports.nginx.listenDual 80}
      ${pkgs.leUtils.nginxWellKnown leDir}

      location / {
        return 301 https://${domain}$request_uri;
      }
    }
  '';

  leHooks = pkgs.leUtils.renewHooks exports domain ++ [
    exports.nginx.reloadCommand
  ];

  processes = {
    foomail = "${pkgs.runit}/bin/chpst -u www-data ${foomail}/bin/foomail";
    letsencrypt = "${pkgs.leUtils.renew [domain] leDir leHooks}";
  };

in {

  exports = {
    letsencrypt.certs.${domain} = leDir;
    nginx.http-include = nginxConfigLink;
  };

  options.domain = mkOption { type = str; };

  run = pkgs.writeBash "foomail" ''

    ${pkgs.leUtils.selfSignCert leDir}

    ${pkgs.coreutils}/bin/mkdir --parents ${logDir}
    ${pkgs.coreutils}/bin/ln --force --symbolic ${nginxConfig} ${nginxConfigLink}
    ${exports.nginx.reloadCommand}

    exec ${pkgs.runsvdir home processes};
  '';
}
